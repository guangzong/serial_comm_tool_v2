//
// Created by zong on 5/18/22.
//

#include "main_window.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QChartView>
#include <QWidget>

static bool check_exist(QString str) {
    for (auto &port: QSerialPortInfo::availablePorts()) {
        if (str == port.portName())
            return true;
    }
    return false;
}

QTextStream receivefileStream;


void main_window::config_enable(bool ena) {
    portsComboBox->setEnabled(ena);
    stopbitsComboBox->setEnabled(ena);
    baudLineEdit->setEnabled(ena);
    dataBitsComboBox->setEnabled(ena);
    tmpFilenameEdit->setEnabled(ena);
    portUpdBtn->setEnabled(ena);
    if (ena) {
        portOpenBtn->setText("open"), is_open = false;
        if (receivefile.isOpen())
            receivefile.close();
        if (rawDataFile.isOpen())
            rawDataFile.close();
    } else {
        portOpenBtn->setText("close"), is_open = true;
        auto filename = tmpFilenameEdit->text();
        receivefile.setFileName(filename);
        receivefile.open(QIODevice::WriteOnly);
        receivefileStream.setDevice(&receivefile);
        rawDataFile.setFileName(filename + "raw");
        rawDataFile.open(QIODevice::WriteOnly);
    }

}

void main_window::setUiComponent() {
    this->ui = new Ui::MainWindow;
    ui->setupUi(this);
    portsComboBox = ui->portsComboBox;
    stopbitsComboBox = ui->stopbitsComboBox;
    baudLineEdit = ui->baudLineEdit;
    dataBitsComboBox = ui->dataBitsComboBox;
    tmpFilenameEdit = ui->tmpFilenameEdit;

    hexTextEdit = ui->hexTextEdit;
    asciiTextEdit = ui->asciiTextEdit;

    plotChartView = ui->plotChartView;

    portOpenBtn = ui->portOpenBtn;
    portUpdBtn = ui->portUpdBtn;
    statusbar = ui->statusbar;

    QObject::connect(portOpenBtn, SIGNAL(clicked()), this, SLOT(open_port()));
    QObject::connect(portUpdBtn, SIGNAL(clicked()), this, SLOT(update_ui_port()));

}

void main_window::port_error_handler(QSerialPort::SerialPortError err) {
    switch (err) {
        case QSerialPort::NoError:
            break;
        case QSerialPort::DeviceNotFoundError:
        case QSerialPort::PermissionError:
        case QSerialPort::OpenError:
        case QSerialPort::WriteError:
        case QSerialPort::ReadError:
        case QSerialPort::ResourceError:
        case QSerialPort::UnsupportedOperationError:
        case QSerialPort::UnknownError:
        case QSerialPort::TimeoutError:
        case QSerialPort::NotOpenError:
            this->config_enable(true);
    }
}

main_window::main_window(QWidget *parent) : QMainWindow(parent) {
    setUiComponent();
    QObject::connect(&port, SIGNAL(readyRead()), this, SLOT(read_data()));
    QObject::connect(&port, &QSerialPort::errorOccurred, this, &main_window::port_error_handler);
//    QFont font("Courier");
//    hexTextEdit->setFont(font);
    QSerialPortInfo info;
    auto tmp = QSerialPortInfo::availablePorts();
    portsComboBox->clear();
    for (auto &port: tmp) {
        qDebug() << port.portName();
        portsComboBox->addItem(port.portName());
    }
    hexTextEdit->setLineWrapMode(QTextEdit::FixedColumnWidth);
    hexTextEdit->setLineWrapColumnOrWidth(48);
    hexTextEdit->setWordWrapMode(QTextOption::WordWrap);
    hexTextEdit->setFontFamily("Mono");

}

main_window::~main_window() {
    delete ui;
}

void main_window::update_ui_port() {
    portsComboBox->clear();
    for (auto &port: QSerialPortInfo::availablePorts()) {
        portsComboBox->addItem(port.portName());
    }
}

void main_window::open_port() {
    if (port.isOpen()) {
        port.close();
        config_enable(true);
        statusbar->showMessage("close port success", 500);
        return;
    }
    if (portsComboBox->count() == 0) { return; }
    auto cur_port = portsComboBox->currentText();
    if (!check_exist(cur_port)) return;
    port.setPortName(cur_port);

    auto stopbits = stopbitsComboBox->currentText();
    if (stopbits == QString("1")) port.setStopBits(QSerialPort::OneStop);
    else if (stopbits == QString("1.5")) port.setStopBits(QSerialPort::OneAndHalfStop);
    else if (stopbits == QString("2")) port.setStopBits(QSerialPort::TwoStop);
    else {
        msgBox.setText("wrong stopbits settings"), msgBox.exec();
        return;
    }
    // TODO: read info from config
    //get baudrate
    port.setBaudRate(baudLineEdit->text().toInt());

    // get databits
    auto databits = dataBitsComboBox->currentText();
    if (databits == QString("5")) port.setDataBits(QSerialPort::Data5);
    else if (databits == QString("6")) port.setDataBits(QSerialPort::Data6);
    else if (databits == QString("7")) port.setDataBits(QSerialPort::Data7);
    else if (databits == QString("8"))
        port.setParity(QSerialPort::NoParity);
    port.setFlowControl(QSerialPort::NoFlowControl);
    if (port.open(QIODevice::ReadWrite)) {
        config_enable(false);
        statusbar->showMessage("open port success", 500);
    } else {
        config_enable(true);
    }

}


double t = 0;
void main_window::read_data() {
    static qint64 val = 0;
    auto data = port.readAll();
    // save data to file
    receivefileStream << data;
//    auto pre_cursor = hexTextEdit->textCursor();
//    QString new_text = QString(data.toHex(' ').toUpper()) + " ";
//    hexTextEdit->moveCursor(QTextCursor::End);
//    bool move_to_end = pre_cursor == hexTextEdit->textCursor();
//    hexTextEdit->insertPlainText(new_text);
//    if (!move_to_end) hexTextEdit->setTextCursor(pre_cursor);
//    auto tmp = hexTextEdit->toPlainText();
//
//    asciiTextEdit->setText(asciiTextEdit->toPlainText() + QString(data));
//    // move cursor to end
//    asciiTextEdit->moveCursor(QTextCursor::End);
//    // clean up ascii text nad hex text if too long
//    if (asciiTextEdit->toPlainText().size() > 10000) {
//        asciiTextEdit->setText(asciiTextEdit->toPlainText().right(1000));
//    }
//    if (hexTextEdit->toPlainText().size() > 10000) {
//        hexTextEdit->setText(hexTextEdit->toPlainText().right(1000));
//    }
    // parse data to line (ascii data)
//    auto lines = QString(data).split('\n');

//    // if line contains 5 float number, then give the first one to plot
//    for (auto &line: lines) {
//        auto tmp = line.split(' ');
////        if (tmp.size() != 5) continue;
//        //  convert first number to float (check if it is a number)
//        bool ok = false;
//        auto data = tmp[0].toFloat(&ok);
//        if (!ok) continue;
//        if (data > 6) data=0.0;
//        t += 1;
//        plotChartView->append(t, data);
//        qDebug() << data;
//    }

}