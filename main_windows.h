////
//// Created by zong on 5/18/22.
////
//
//#ifndef UNTITLED_MAIN_WINDOWS_H
//#define UNTITLED_MAIN_WINDOWS_H
//
//#include "ui_main.h"
//#include <QSerialPort>
//#include <QThread>
//
//class main_windows : public QMainWindow {
//Q_OBJECT
//public:
//    explicit main_windows(QWidget *parent = nullptr);
//
//    ~main_windows() override;
//
//private:
//    Ui::MainWindow *ui;
//    bool is_open = false;
//
//
//    QSerialPort port;
//
//public slots:
//
//    void update_ui_port();
//
//    void open_port();
//
//    void read_data();
//};
//
//
//#endif //UNTITLED_MAIN_WINDOWS_H
