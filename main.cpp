#include <QApplication>
#include <QPushButton>
#include "main_window.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    main_window win;
    win.read_data();
    win.show();
    return QApplication::exec();
}
