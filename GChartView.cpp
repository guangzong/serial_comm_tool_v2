//
// Created by zong on 6/10/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_gchartview.h" resolved

#include "GChartView.h"
#include "QtCharts/QScatterSeries"
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>


#define MAX_DATA 200
GChartView::GChartView(QWidget *parent) : QChartView(parent) {
    data = new QSplineSeries();
    // Customize spline series
    data->setPen(QPen(Qt::blue)); // Set the color of the spline line

// Customize scatter series
//    data->setMarkerSize(10.0); // Set the size of the scatter points
    data->setPointsVisible(true); // Set the scatter points visible or not
    data->setBrush(QBrush(Qt::red)); // Set the color of the scatter points

    axis_x = new QValueAxis();
    axis_y = new QValueAxis();
    chart = new QChart();
    chart->setTitle("WIP");
    axis_x->setRange(0, MAX_DATA);
    axis_y->setRange(-6, 7);
//    axis_x->setTickType(QValueAxis::TickType::TicksDynamic);
    axis_x->setTickInterval(1);
    axis_x->setLabelFormat("%d");
//    axis_y->setTickCount(21);
    chart->addAxis(axis_x, Qt::AlignBottom);
    chart->addAxis(axis_y, Qt::AlignLeft);
    chart->addSeries(data);
    this->setChart(chart);
    data->attachAxis(axis_x);
    data->attachAxis(axis_y);
}

GChartView::~GChartView() {
}

void GChartView::wheelEvent(QWheelEvent *event) {
//    auto degrees = event->angleDelta();
//    auto cursor_y = event->position().y();
//    auto cur_range_max = axis_y->max();
//    auto cur_range_min = axis_y->min();
//
//    // left and top corner is origin
//    auto pos_max = chart->mapToPosition(QPointF(0, cur_range_min)).y();
//    auto pos_min = chart->mapToPosition(QPointF(0, cur_range_max)).y();
//    auto new_range = 5.0;
//    auto center = (qreal) 0.0;
//    if (cursor_y < pos_max && cursor_y > pos_min) {
//        center = chart->mapToValue(QPointF(0, cursor_y)).y();
//        auto cur_range = (cur_range_max - cur_range_min);
//        if (degrees.y() > 0)
//            new_range = cur_range * 0.8;
//        else
//            new_range = cur_range / 0.8;
//    }
//    // setup new axis
//    auto new_max = (center + new_range / 2.0);
//    auto new_min = (center - new_range / 2.0);
//    if (new_max > 2.5) {
//        new_min -= (new_max - 2.5);
//        new_max = 2.5;
//    }
//    if (new_min < -2.5) {
//        new_max += (-2.5 - new_min);
//        new_min = -2.5;
//        new_max = new_max > 2.5 ? 2.5 : new_max;
//    }
//
//    axis_y->setRange(new_min, new_max);
//    qDebug() << "new_min:  " << new_min << " " << new_max;
}

void GChartView::append(qreal x, qreal y) {
    data->append(x, y);
    while (data->count() > MAX_DATA) {
        data->remove(0);
    }
    auto end = data->at(data->count()-1);
    if (end.x() > MAX_DATA) {
        axis_x->setRange(end.x()-MAX_DATA, end.x());
    }

}
