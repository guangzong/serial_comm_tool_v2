//
// Created by zong on 6/10/22.
//

#ifndef COM_GCHARTVIEW_H
#define COM_GCHARTVIEW_H

#include <QChartView>

#include <QLineSeries>
#include <QValueAxis>
#include <QChart>
#include <QSplineSeries>

QT_BEGIN_NAMESPACE
namespace Ui { class gchartview; }
QT_END_NAMESPACE

class GChartView : public QChartView {
Q_OBJECT

public:
    explicit GChartView(QWidget *parent = nullptr);
    ~GChartView() override;
    void wheelEvent(QWheelEvent *even) override;

private:
//    QLineSeries* data;
    QSplineSeries *data;
    QValueAxis* axis_x;
    QValueAxis* axis_y;
    QChart* chart;
public:

    void append(qreal x, qreal y);

};


#endif //COM_GCHARTVIEW_H
