//
// Created by zong on 5/18/22.
//

#ifndef UNTITLED_MAIN_WINDOW_H
#define UNTITLED_MAIN_WINDOW_H

#include "ui_main.h"
#include <QSerialPort>
#include <QFile>
#include <QThread>
#include <QLineSeries>
#include <QMessageBox>
#include <QSplineSeries>
#include <QValueAxis>

//typedef struct _Glinechart{
//    QLineSeries* data;
//    QValueAxis* axis_x;
//    QValueAxis* axis_y;
//    QChart* chart;
//    _Glinechart() {
//        data = new QLineSeries();
//        axis_x = new QValueAxis();
//        axis_y = new QValueAxis();
//        chart = new QChart();
//    }
//}GLineChart;

class main_window : public QMainWindow {
Q_OBJECT
public:
    explicit main_window(QWidget *parent = nullptr);

    ~main_window() override;

private:
    Ui::MainWindow *ui;
    bool is_open = false;
    QSerialPort port;
    QFile receivefile;
    QFile rawDataFile;

    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QComboBox *stopbitsComboBox;
    QComboBox *dataBitsComboBox;
    QPushButton *portUpdBtn;
    QLabel *label_4;
    QComboBox *portsComboBox;
    QLineEdit *tmpFilenameEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *portOpenBtn;
    QLabel *label_3;
    QLineEdit *baudLineEdit;
    QSpacerItem *verticalSpacer;
    QTextEdit *hexTextEdit;
    QTextEdit *asciiTextEdit;
    QMenuBar *menubar;
    QStatusBar *statusbar;
    GChartView *plotChartView;

    QMessageBox msgBox;
//    GLineChart lineChart;

private:
    void setUiComponent();

public slots:

    void update_ui_port();

    void open_port();

    void read_data();

    void port_error_handler(QSerialPort::SerialPortError err);

    void config_enable(bool ena);
};


#endif //UNTITLED_MAIN_WINDOW_H
